# niva-gitlab-guidelines

Guidelines for repositories published by the NIVA project

# Introduction

This sub-project is part of the [New IACS Vision in Action --- NIVA](https://www.niva4cap.eu/) project that delivers a a suite of digital solutions, e-tools 
and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union&#39;s Horizon 2020 research and innovation programme under

grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

# Guidelines

Guidelines are published on the wiki section, so visit the wiki
